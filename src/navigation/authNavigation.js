import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Home from '../screen/Home';
import Detail from '../screen/Detail';
import Edit from '../screen/Edit';

const Stack = createStackNavigator();

export default function authNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Detail" component={Detail} />
      <Stack.Screen name="Edit" component={Edit} />
    </Stack.Navigator>
  );
}
