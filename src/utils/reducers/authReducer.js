const initialState = {
  dataRedux: {},
};
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'POST':
      return {
        ...state,
        dataRedux: action.data,
      };
    default:
      return state;
  }
};

export default authReducer;
