import React from 'react';
import {View, Modal, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {TextBold, TextRegular} from '../global/Text';
import {Colors} from '../../styles';
import Icon from 'react-native-vector-icons/AntDesign';
import {opacityColor} from '../../utils/Helper';

const ModalBottom = ({
  show,
  onClose,
  textLeft = 'Batalkan',
  textRight = 'Selesai',
  textCenter = 'Kontak Baru',
  children,
  onSave,
}) => {
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={styles.header}>
            <TouchableOpacity onPress={onClose}>
              <TextBold text={textLeft} color={Colors.GOLD} size={16} />
            </TouchableOpacity>
            <TextBold text={textCenter} size={16} color={Colors.GOLD} />
            <TouchableOpacity onPress={onSave}>
              <TextBold text={textRight} color={Colors.GOLD} size={16} />
            </TouchableOpacity>
          </View>
          {children}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // borderBottomColor: '#E4E7EB',
    // borderBottomWidth: 1,
  },
  body: {
    width: '100%',
    backgroundColor: '#2d2e2e',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});

export default ModalBottom;
