import {
  FlatList,
  Image,
  Modal,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {Header, InputText, TextBold, TextRegular} from '../global';
import {Colors} from '../../styles';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import {opacityColor} from '../../utils/Helper';
import ModalBottom from '../modal/ModalBottom';

const HomeComponent = ({
  offline,
  data,
  namaDepan,
  setNamaDepan,
  namaBelakang,
  setNamaBelakang,
  umur,
  setUmur,
  foto,
  onSave,
  navigation,
}) => {
  const [showModal, setShowModal] = useState(false);
  return (
    <View>
      <ModalBottom
        show={showModal}
        onClose={() => setShowModal(false)}
        onSave={() => {
          const value = {
            firstName: namaDepan,
            lastName: namaBelakang,
            age: umur,
          };
          onSave(value);
          setShowModal(false);
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 16,
          }}>
          <Image
            source={require('../../asset/image/profile.png')}
            style={{height: 150, width: 150, borderRadius: 150 / 2}}
          />
          <TouchableOpacity style={{marginVertical: 8}}>
            <TextRegular text="Tambah Foto" color={Colors.GOLD} />
          </TouchableOpacity>
          <View style={styles.textInput}>
            <InputText
              placeholderText="Nama Depan"
              placeholderTextColor={opacityColor(Colors.GOLD, 5)}
              value={namaDepan}
              onChangeText={text => setNamaDepan(text)}
            />
            <View style={{backgroundColor: Colors.GREY, height: 1}} />
            <InputText
              placeholderText="Nama Belakang"
              placeholderTextColor={opacityColor(Colors.GOLD, 5)}
              value={namaBelakang}
              onChangeText={text => setNamaBelakang(text)}
            />
          </View>
          <View style={[styles.textInput, {marginTop: 16}]}>
            <InputText
              placeholderText="Umur"
              placeholderTextColor={opacityColor(Colors.GOLD, 5)}
              value={umur}
              onChangeText={value => setUmur(value)}
              keyboardType="numeric"
            />
          </View>
        </View>
      </ModalBottom>
      <View style={{padding: 16}}>
        <View style={styles.header}>
          <TextBold text="Kontak" size={20} color={Colors.GOLD} />
          {offline == false ? (
            <TouchableOpacity onPress={() => setShowModal(true)}>
              <Icon name="plus" size={22} color={Colors.GOLD} />
            </TouchableOpacity>
          ) : null}
        </View>
        <View style={styles.search}>
          <Icon name="search1" size={20} color={Colors.GOLD} />
          <InputText
            placeholderText="Cari..."
            placeholderTextColor={opacityColor(Colors.GOLD, 5)}
          />
        </View>
      </View>
      <View style={styles.garis} />
      <FlatList
        data={data.data}
        contentContainerStyle={{paddingBottom: 20}}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(item, index) => (
          <View style={{paddingHorizontal: 18}}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Detail', {
                  route: item?.item,
                  offline: offline,
                })
              }
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={{uri: item?.item?.photo}}
                style={{height: 48, width: 48, borderRadius: 34}}
              />
              <TextBold
                text={item?.item?.firstName + ' ' + item?.item?.lastName}
                size={14}
                color={Colors.GOLD}
                style={{marginLeft: 12}}
              />
            </TouchableOpacity>
            <View style={styles.garis} />
          </View>
        )}
      />
    </View>
  );
};

export default HomeComponent;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  search: {
    backgroundColor: opacityColor('#D9D9D9', 2),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 12,
    marginTop: 8,
    borderRadius: 10,
  },
  garis: {
    backgroundColor: Colors.GREY,
    height: 1,
    marginVertical: 12,
    // marginTop: 12,
  },
  textInput: {
    backgroundColor: opacityColor(Colors.SILVER, 2),
    paddingLeft: 16,
    width: '100%',
    marginTop: 6,
  },
});
