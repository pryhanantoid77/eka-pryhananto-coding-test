import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Header, TextBold, TextMedium} from '../global';
import {Colors} from '../../styles';
import {opacityColor} from '../../utils/Helper';

const DetailComponent = ({navigation, data, onDelete}) => {
  return (
    <View>
      <Header
        textLeft="Kontak"
        textLeftSize={16}
        textLeftColor={Colors.GOLD}
        onPressBack={() => navigation.navigate('Home')}
        onPressLeft={() => navigation.navigate('Home')}
        iconBackColor={Colors.GOLD}
      />
      <View
        style={{padding: 16, justifyContent: 'center', alignItems: 'center'}}>
        <Image
          source={{uri: data?.route?.photo}}
          style={{height: 80, width: 80, borderRadius: 80 / 2}}
        />
        <TextBold
          text={data?.route?.firstName + ' ' + data?.route?.lastName}
          size={20}
          color={Colors.GOLD}
          style={{marginVertical: 12}}
        />
        <View style={styles.data}>
          <TextBold text="Age" color={Colors.GOLD} />
          <TextMedium text={data?.route?.age} color={Colors.GOLD} />
          <View style={styles.garis} />
          <TextBold text="No Telepon" color={Colors.GOLD} />
          {/* <TextMedium text={data?.route?.age} color={Colors.GOLD} /> */}
        </View>
        {data?.offline == false ? (
          <View style={{width: '100%'}}>
            {console.log('log navigation edit', data.route)}
            <TouchableOpacity
              onPress={() => navigation.navigate('Edit', data.route)}
              style={[styles.data, {marginTop: 12}]}>
              <TextBold text="Edit" size={20} color={Colors.GOLD} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => onDelete()}
              style={[styles.data, {marginTop: 12}]}>
              <TextBold text="Hapus" size={20} color="#FF0000" />
            </TouchableOpacity>
          </View>
        ) : (
          <View style={{width: '100%'}}>
            <View style={[styles.dataOff, {marginTop: 12}]}>
              <TextBold
                text="Edit"
                size={20}
                color={opacityColor(Colors.GOLD, 2)}
              />
            </View>
            <View style={[styles.dataOff, {marginTop: 12}]}>
              <TextBold
                text="Hapus"
                size={20}
                color={opacityColor('#FF0000', 2)}
              />
            </View>
          </View>
        )}
      </View>
    </View>
  );
};

export default DetailComponent;

const styles = StyleSheet.create({
  garis: {
    backgroundColor: Colors.GREY,
    height: 1,
    marginVertical: 12,
    // marginTop: 12,
  },
  data: {
    backgroundColor: opacityColor(Colors.SILVER, 2),
    paddingVertical: 16,
    paddingLeft: 16,
    width: '100%',
    borderRadius: 8,
  },
  dataOff: {
    borderColor: opacityColor(Colors.SILVER, 2),
    borderWidth: 1,
    paddingVertical: 16,
    paddingLeft: 16,
    width: '100%',
    borderRadius: 8,
  },
});
