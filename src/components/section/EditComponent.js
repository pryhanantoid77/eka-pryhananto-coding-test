import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Header, InputText, TextRegular} from '../global';
import {Colors} from '../../styles';
import {opacityColor} from '../../utils/Helper';

const EditComponent = ({
  navigation,
  namaDepan,
  setNamaDepan,
  namaBelakang,
  setNamaBelakang,
  umur,
  setUmur,
  foto,
  setFoto,
  onSave,
}) => {
  return (
    <View>
      <Header
        textLeft="Batalkan"
        textLeftColor={Colors.GOLD}
        textLeftSize={16}
        iconRight={true}
        textRight="Selesai"
        onPressRight={() => {
          onSave();
        }}
        textRightColor={Colors.GOLD}
        textRightSize={16}
        onPressLeft={() => navigation.pop()}
        textCenter="Edit Kontak"
      />
      <View
        style={{
          padding: 16,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={{uri: foto}}
          style={{height: 150, width: 150, borderRadius: 150 / 2}}
        />
        <TouchableOpacity style={{marginVertical: 8}}>
          <TextRegular text="Tambah Foto" color={Colors.GOLD} />
        </TouchableOpacity>
        <View style={styles.textInput}>
          <InputText
            placeholderText="Nama Depan"
            placeholderTextColor={opacityColor(Colors.GOLD, 5)}
            value={namaDepan}
            onChangeText={text => setNamaDepan(text)}
          />
          <View style={{backgroundColor: Colors.GREY, height: 1}} />
          <InputText
            placeholderText="Nama Belakang"
            placeholderTextColor={opacityColor(Colors.GOLD, 5)}
            value={namaBelakang}
            onChangeText={text => setNamaBelakang(text)}
          />
        </View>
        <View style={[styles.textInput, {marginTop: 16}]}>
          <InputText
            placeholderText="Umur"
            placeholderTextColor={opacityColor(Colors.GOLD, 5)}
            value={umur.toString()}
            onChangeText={text => setUmur(text)}
            keyboardType="numeric"
          />
        </View>
      </View>
    </View>
  );
};

export default EditComponent;

const styles = StyleSheet.create({
  textInput: {
    backgroundColor: opacityColor(Colors.SILVER, 2),
    paddingLeft: 16,
    width: '100%',
    marginTop: 6,
  },
});
