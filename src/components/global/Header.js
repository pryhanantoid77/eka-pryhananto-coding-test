import React from 'react';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {TextBold, TextMedium, TextRegular} from './Text';
import {Colors} from '../../styles';
import Icon from 'react-native-vector-icons/AntDesign';

export const Header = ({
  textLeft = 'Header',
  textLeftSize = 14,
  textLeftColor = Colors.DARKBLUE,
  onPressBack,
  iconBackColor = Colors.DARKBLUE,
  textRight = 'Header',
  textRightSize = 14,
  textRightColor = Colors.DARKBLUE,
  onPressRight,
  backgroundHeader = Colors.WHITE,
  titleCenter = true,
  iconRight,
  onPressLeft,
  textCenter,
}) => {
  return (
    <View
      style={[
        styles.container,
        {
          backgroundColor: backgroundHeader,
        },
      ]}>
      <View
        style={{
          //   width: '80%',
          //   justifyContent: titleCenter ? 'center' : 'flex-start',
          //   alignItems: titleCenter ? 'center' : 'flex-start',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        {onPressBack && (
          <TouchableOpacity
            onPress={onPressBack}
            style={{
              // width: '10%',
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              marginRight: 5,
            }}>
            <Icon name="left" size={18} color={iconBackColor} />
          </TouchableOpacity>
        )}
        <TouchableOpacity onPress={onPressLeft}>
          <TextBold text={textLeft} size={textLeftSize} color={textLeftColor} />
        </TouchableOpacity>
      </View>
      <TextBold text={textCenter} size={textLeftSize} color={textLeftColor} />

      {iconRight && (
        <TouchableOpacity
          onPress={onPressRight}
          style={{alignSelf: 'flex-end'}}>
          <TextBold
            text={textRight}
            size={textRightSize}
            color={textRightColor}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: 'space-between',
  },
});
