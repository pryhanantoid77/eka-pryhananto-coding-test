import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import {Colors} from '../../styles';

export const InputText = ({
  placeholderText = 'Masukkan Email',
  placeholderTextColor = 'grey',
  multiline = false,
  numberOfLines = 1,
  onChangeText,
  value,
  style,
  keyboardType = 'default',
  edittable = true,
  selectTextOnFocus = true,
  showPassword,
}) => {
  return (
    <View style={[styles.container, style]}>
      <TextInput
        placeholder={placeholderText}
        placeholderTextColor={placeholderTextColor}
        style={{
          width: '90%',
          paddingHorizontal: 10,
          color: Colors.GOLD,
        }}
        multiline={multiline}
        numberOfLines={numberOfLines}
        secureTextEntry={showPassword}
        value={value}
        onChangeText={text => onChangeText(text)}
        keyboardType={keyboardType}
        editable={edittable}
        selectTextOnFocus={selectTextOnFocus}
        defaultValue=""
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
