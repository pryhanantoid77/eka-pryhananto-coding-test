import {RefreshControl, ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Colors} from '../styles';
import EditComponent from '../components/section/EditComponent';
import apiProvider from '../utils/service/apiProvider';

const Edit = ({navigation, route}) => {
  const [namaDepan, setNamaDepan] = useState('');
  const [namaBelakang, setNamaBelakang] = useState('');
  const [umur, setUmur] = useState('');
  const [foto, setFoto] = useState('');
  console.log('edit route', route.params);

  const data = route.params;
  useEffect(() => {
    setNamaDepan(data?.firstName);
    setNamaBelakang(data?.lastName);
    setUmur(data?.age);
    setFoto(data?.photo);
  }, []);
  const editData = async () => {
    console.log('masuk editdata');
    const dataEdit = {
      firstName: namaDepan,
      lastName: namaBelakang,
      age: umur,
      photo: foto,
    };
    console.log('dataEdit', dataEdit);
    const res = await apiProvider.editData(data.id, dataEdit);
    if (res.status == 201 || res.status == 200) {
      navigation.navigate('Detail', dataEdit);
    }
    console.log('res', res);
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.BLACK}}>
      <EditComponent
        navigation={navigation}
        namaDepan={namaDepan}
        namaBelakang={namaBelakang}
        umur={umur}
        setNamaDepan={text => setNamaDepan(text)}
        setNamaBelakang={text => setNamaBelakang(text)}
        setUmur={value => setUmur(value)}
        foto={foto}
        setFoto={value => setFoto(value)}
        onSave={editData}
      />
    </View>
  );
};

export default Edit;

const styles = StyleSheet.create({});
