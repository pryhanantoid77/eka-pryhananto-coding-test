import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Colors} from '../styles';
import DetailComponent from '../components/section/DetailComponent';
import apiProvider from '../utils/service/apiProvider';

const Detail = ({navigation, route}) => {
  console.log('route', route);
  const idDetail = route.params.route.id;
  const handleHapus = async () => {
    console.log('masuk delete', route.params.route.id, route.params.route.id);
    const id = route.params.route.id;
    console.log('id', id);
    const data = {
      id: idDetail,
    };
    console.log('data', data);
    const res = await apiProvider.deleteData(id, data);
    console.log(res);
    if (res.response.status === 200) {
      navigation.navigate('Home');
    }
  };
  return (
    <View style={{flex: 1, backgroundColor: Colors.BLACK}}>
      <DetailComponent
        navigation={navigation}
        data={route.params}
        onDelete={handleHapus}
      />
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({});
