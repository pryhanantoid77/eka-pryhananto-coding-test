import {
  KeyboardAvoidingView,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import HomeComponent from '../components/section/HomeComponent';
import {Colors} from '../styles';
import apiProvider from '../utils/service/apiProvider';
import {useIsFocused} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';

const Home = ({navigation}) => {
  const [data, setData] = useState([]);
  const [namaDepan, setNamaDepan] = useState('');
  const [namaBelakang, setNamaBelakang] = useState('');
  const [umur, setUmur] = useState(0);
  const [foto, setFoto] = useState('');
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const {dataRedux} = useSelector(state => state.auth);
  const [refreshing, setRefreshing] = useState(false);
  const [offline, setOffline] = useState(true);

  const handleRefresh = () => {
    setRefreshing(true);
    getData();
  };
  useEffect(() => {
    getData();
  }, [isFocused]);

  const getData = async () => {
    const response = await apiProvider.getData();
    console.log('response', response);
    if (response.message == 'Get contacts') {
      setData(response);
      const contact = response;
      console.log('contact', contact);
      var dataContact = contact;
      // const data = {
      //   contact,
      // };
      // dataContact.push(data);
      dispatch({type: 'POST', data: dataContact});
      console.log('Masuk Sini');
      setOffline(false);
    } else if (dataRedux.message == 'Get contacts') {
      setData(dataRedux);
    } else {
      getData();
    }
    setRefreshing(false);
  };

  const onSave = async value => {
    console.log('value onSave', value);
    const data = {
      ...value,
      photo:
        'http://vignette1.wikia.nocookie.net/lotr/images/6/68/Bilbo_baggins.jpg/revision/latest?cb=20130202022550',
    };
    const response = await apiProvider.postData(data);
    getData();
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.BLACK}}>
      {console.log('reduxxxx', dataRedux)}
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={handleRefresh} />
        }>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <HomeComponent
            offline={offline}
            data={data}
            namaDepan={namaDepan}
            setNamaDepan={text => setNamaDepan(text)}
            namaBelakang={namaBelakang}
            setNamaBelakang={text => setNamaBelakang(text)}
            umur={umur}
            setUmur={value => setUmur(value)}
            foto={foto}
            onSave={onSave}
            navigation={navigation}
          />
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({});
